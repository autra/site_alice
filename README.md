# What is this?

This is a wordpress custom template for one of my friend (you can pay her a
visit at http://alicefagard.fr). You can safely go elsewhere on the Internet
now.

That being said, if you notice issues, especially in browser compatibility,
please do report them, I'll be grateful.

# Plugin to activate before going to prod

- disable-google-fonts 
- disable-emojis
- qTranslate-x
- foobox
- Foo gallery
