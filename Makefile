alice.zip:
	git archive --format zip --output alice.zip HEAD

deploy-preprod: alice.zip
	scp alice.zip site_alice:.
	# remove old theme, unzip new uploaded theme in the right location and cleanup
	ssh site_alice 'rm -rf ~/public_html/preprod/wp-content/themes/alicefa && \
		unzip alice.zip -d ~/public_html/preprod/wp-content/themes/alicefa && \
		rm alice.zip'
	rm alice.zip

deploy-prod: alice.zip
	scp alice.zip site_alice:.
	# remove old theme, unzip new uploaded theme in the right location and cleanup
	ssh site_alice 'rm -rf ~/public_html/wp-content/themes/alicefa && \
		unzip alice.zip -d ~/public_html/wp-content/themes/alicefa && \
		rm alice.zip'
	rm alice.zip
