<?php
/**
 * I don't like the html structure of the password form, so let's do a custom
 * one.
 */
add_filter('the_password_form', 'custom_the_password_form');

function custom_the_password_form() {
  $post = get_post();
  $label = 'pwbox-' . ( empty($post->ID) ? rand() : $post->ID );

  // it's confusing, but apparently, wordpress adds some <br> and <p> tags if I
  // put too many \n in the string, thus the frequent concat operator.
  $output =
    '<form action="' .
    esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) .
    '" class="post-password-form" method="post">
      <label for="' . $label . '">' .
        __( 'This content is password protected. To view it please enter your password below:' ) .
      '</label>'.
      '<div class="post-password-container">'.
        '<input class="post-password-submit" type="submit" name="Submit" value="' .
          esc_attr_x( 'Enter', 'post password form' ) . '"/>'.
        '<div class="password-container">'.
          '<input name="post_password" id="' . $label . '" type="password" size="20" />'.
        '</div>'.
      '</div>'.
    '</form>';

  return $output;
}

/**
 * My widget area is actually in the header (at least for the language
 * switcher), and the footer
 */

function alicefa_widgets_init() {

  // unregistering sidebar, since there is no sidebar
  unregister_sidebar( 'sidebar-1' );
  // at the moment, only one sidebar at the bottom
  unregister_sidebar( 'sidebar-3' );

  register_sidebar( array(
    'name'          => __( 'Content Top', 'alicefa' ),
    'id'            => 'sidebar-4',
    'description'   => __( 'Appear in the header in the right side.', 'alicefa' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );
}
add_action( 'widgets_init', 'alicefa_widgets_init', 11 );


function alicefa_scripts() {
  wp_dequeue_style( 'twentysixteen-ie' );
  wp_dequeue_style('twentysixteen-ie7');
  wp_dequeue_style('twentysixteen-ie8');

  //  for flexbox.
  wp_enqueue_style( 'alicefa_ie', get_stylesheet_directory_uri() . '/css/ie.css', array('twentysixteen-style'));
  wp_style_add_data( 'alicefa_ie', 'conditional', 'lte IE 9' );
}
add_action( 'wp_enqueue_scripts', 'alicefa_scripts', 100 );

?>
